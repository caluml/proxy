package proxy;

public interface ProxyRequest {

  void handle() throws Exception;
}
