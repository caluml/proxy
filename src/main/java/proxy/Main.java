package proxy;

public class Main {

  public static void main(String... args) throws Exception {
    Proxy proxy = new Proxy(8080);

    proxy.start();

    Thread.sleep(Long.MAX_VALUE);
  }
}
