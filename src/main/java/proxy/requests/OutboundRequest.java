package proxy.requests;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import proxy.ProxyRequest;

import java.io.*;
import java.util.Map;

public class OutboundRequest implements ProxyRequest, Runnable {

  private final Map<String, String> headers;
  private final InputStream clientRequest;
  private final OutputStream clientResponse;
  private final HttpRequestBase request;

  public OutboundRequest(HttpRequestBase request,
                         Map<String, String> headers,
                         InputStream clientRequest,
                         OutputStream clientResponse) {
    this.request = request;
    this.headers = headers;
    this.clientRequest = clientRequest;
    this.clientResponse = clientResponse;
  }

  @Override
  public void run() {
    handle();
  }

  @Override
  public void handle() {
    try {
      CloseableHttpClient httpClient = HttpClientBuilder.create().build();

      for (Map.Entry<String, String> header : headers.entrySet()) {
        request.addHeader(header.getKey(), header.getValue());
      }
      System.out.println("Added " + headers);

      if (request instanceof HttpPost) {
        System.out.println("Setting request body");
        if (headers.containsKey("Content-Length")) {
          int contentLength = Integer.parseInt(headers.get("Content-Length"));

          System.out.println(clientRequest.read());
          System.out.println(clientRequest.read());
          System.out.println(clientRequest.read());
          System.out.println(clientRequest.read());
          System.out.println(clientRequest.read());
          System.out.println(clientRequest.read());

          char[] chars = new char[contentLength];
          new InputStreamReader(clientRequest).read(chars);
          System.out.println("Read " + chars);
        }

        HttpEntity body = new StringEntity(IOUtils.toString(clientRequest), "UTF-8");
        ((HttpPost) request).setEntity(body);
      }

      System.out.println("Executing " + request);
      CloseableHttpResponse httpResponse = httpClient.execute(request);

      InputStream serverResponse = httpResponse.getEntity().getContent();
      IOUtils.copy(serverResponse, clientResponse);

    } catch (Exception e) {
      e.printStackTrace(System.err);
    } finally {
      try {
        clientResponse.flush();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      try {
        clientResponse.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  private String readLine(InputStream inputStream) throws IOException {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    while (true) {
      int read = inputStream.read();
      if (read == 13) break;
      byteArrayOutputStream.write(read);
    }
    return new String(byteArrayOutputStream.toByteArray());
  }

}
