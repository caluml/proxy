package proxy.requests;

import org.apache.commons.io.IOUtils;
import proxy.ProxyRequest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

/**
 * Handles an SSL CONNECT request. No MitMing takes place.
 */
public class OutboundConnectRequest implements ProxyRequest, Runnable {

	private static final String PROXY_AGENT = "ProxyServer/1.0";

	private final String host;
	private final int port;
	private final InputStream clientRequest;
	private final OutputStream clientResponse;

	public OutboundConnectRequest(String host,
																int port,
																InputStream clientRequest,
																OutputStream clientResponse) {
		this.host = host;
		this.port = port;
		this.clientRequest = clientRequest;
		this.clientResponse = clientResponse;
	}

	@Override
	public void run() {
		handle();
	}

	@Override
	public void handle() {
		System.out.println(host + ":" + port);
		try (Socket server = new Socket(host, port);
				 InputStream serverResponse = server.getInputStream();
				 OutputStream serverRequest = server.getOutputStream()) {

			String proxyHeaders = "HTTP/1.0 200 Connection established\r\n" +
				"Proxy-Agent: " + PROXY_AGENT + "\r\n" +
				"\r\n";
			clientResponse.write(proxyHeaders.getBytes());

			final int[] written = new int[1];

			new Thread(() -> {
				try {
					written[0] = IOUtils.copy(clientRequest, serverRequest);
				} catch (SocketException ignored) {
				} catch (IOException e) {
					e.printStackTrace(System.err);
				}
			}).start();

			int read = IOUtils.copy(serverResponse, clientResponse);
			System.out.println("Wrote " + written[0] + " to, read " + read + " bytes from " + host);
		} catch (SocketException ignored) {
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}
}
