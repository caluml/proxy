package proxy;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import proxy.access.Blocklist;
import proxy.requests.OutboundConnectRequest;
import proxy.requests.OutboundRequest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class ConnectionHandler implements Runnable {

	private final Socket socket;
	private final Blocklist blocklist;

	public ConnectionHandler(Socket socket,
													 Blocklist blocklist) {
		this.socket = socket;
		this.blocklist = blocklist;
	}

	@Override
	public void run() {
		try {
			InputStream input = socket.getInputStream();
			OutputStream output = socket.getOutputStream();
			InputStreamReader inputReader = new InputStreamReader(input);
			BufferedReader bufferedReader = new BufferedReader(inputReader);

			String firstLine = bufferedReader.readLine();
			if (firstLine != null) {
//        System.out.println(firstLine);
				Map<String, String> headers = new HashMap<>();
				while (true) {
					String header = bufferedReader.readLine();
					if (header == null || header.isEmpty()) break;
					int i = header.indexOf(": ");
					headers.put(header.substring(0, i), header.substring(i + 2));
				}
//				System.out.println("Request headers: " + headers);

				String[] strings = firstLine.split(" ");
				String verb = strings[0];
				String hostPort = strings[1];
				String version = strings[2];

				String host = getHost(hostPort);
				int port = getPost(hostPort);

				if (blocklist.isBlocked(host, port)) {
					System.out.println("\tBlocking request to " + host + ":" + port);
					output.write("HTTP/1.1 403 Forbidden\r\n\r\n".getBytes());
					output.flush();
					input.close();
					output.close();
					return;
				}

				switch (verb) {
					case "CONNECT":
						new OutboundConnectRequest(host, port, input, output).handle();
						break;
//					case "GET":
//						new OutboundRequest(new HttpGet(hostPort), headers, input, output).handle();
//						break;
//					case "POST":
//						new OutboundRequest(new HttpPost(hostPort), headers, input, output).handle();
//						break;
					default:
						System.err.println("No handler for " + firstLine);
						break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

	private String getHost(String hostPort) {
		try {
//			return hostPort.split("://")[1];
			return hostPort.split(":")[0];
		} catch (Exception e) {
			System.err.println("Error getting host from " + hostPort);
			e.printStackTrace();
			return null;
		}
	}

	private int getPost(String hostPort) {
		try {
			String[] splits = hostPort.split(":");
			if ("http".equals(splits[0])) {
				return 80;
			}
			return Integer.parseInt(splits[1]);
		} catch (Exception e) {
			System.err.println("Error getting port from " + hostPort);
			e.printStackTrace();
			return -1;
		}
	}
}
