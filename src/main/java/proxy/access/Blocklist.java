package proxy.access;

public interface Blocklist {

	boolean isBlocked(String host,
										int port);
}
