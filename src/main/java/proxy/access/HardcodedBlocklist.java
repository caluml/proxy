package proxy.access;

import java.util.Arrays;
import java.util.List;

public class HardcodedBlocklist implements Blocklist {

  private static final List<String> suffixes = Arrays.asList(
    "google-analytics.com",
    "fonts.googleapis.com",
    "doubleclick.net",
    "serverbid.com",
    "admantx.com",
    "newzit.com",
    "scripts.dailymail.co.uk"
  );

  @Override
  public boolean isBlocked(String host, int port) {
    for (String suffix : suffixes) {
      if (host.endsWith(suffix)) return true;
    }

    return false;
  }
}
