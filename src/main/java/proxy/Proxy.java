package proxy;

import proxy.access.Blocklist;
import proxy.access.HardcodedBlocklist;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Proxy {

	private final int port;
	private boolean running = true;

	private final ExecutorService executorService = Executors.newFixedThreadPool(100);

	public Proxy(int port) {
		this.port = port;
	}

	public void start() throws IOException {
		Blocklist blocklist = new HardcodedBlocklist();
		ServerSocket serverSocket = new ServerSocket(port);
		System.out.println("Listening on " + serverSocket);
		while (running) {
			Socket socket = serverSocket.accept();
			executorService.submit(new ConnectionHandler(socket, blocklist));
		}
	}
}
